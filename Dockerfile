# feesmrt/ros-nvidia-moveit-full
# Docker project for creating a ROS image with full moveit installation.
# Additional TRAC_IK from TRACLABS is installed.
# Root image is the feesmrt/ros-nvidia-base
# Updates & Installs MoveIt! full desktop

# Base image with ROS indigo and nvidia hardware accelleration
FROM feesmrt/ros-nvidia-base:indigo

MAINTAINER feesmrt

# Update & upgrade
RUN sudo apt-get -y update

# Install MoveIt! desktop full and ROS control
RUN sudo apt-get -y install \
     ros-indigo-moveit-full \
     ros-indigo-hardware-interface \
     ros-indigo-controller-interface \
     ros-indigo-controller-manager \
     ros-indigo-controller-manager-msgs \
     ros-indigo-controller-manager-tests \
     ros-indigo-control-msgs \
     ros-indigo-control-toolbox \
     ros-indigo-position-controllers \
     ros-indigo-joint-state-controller \
     ros-indigo-joint-trajectory-controller \
     ros-indigo-joint-limits-interface \
     libnlopt-dev \
     ros-indigo-joy 
